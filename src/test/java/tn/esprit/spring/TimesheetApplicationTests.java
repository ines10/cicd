package tn.esprit.spring;

import org.apache.log4j.Logger;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.context.SpringBootTest;
import org.junit.*;
import tn.esprit.spring.entities.Departement;
import tn.esprit.spring.entities.Employe;
import tn.esprit.spring.entities.Entreprise;
import tn.esprit.spring.entities.Role;
import tn.esprit.spring.repository.DepartementRepository;
import tn.esprit.spring.repository.EmployeRepository;
import tn.esprit.spring.repository.EntrepriseRepository;
import tn.esprit.spring.services.DepartementServiceImpl;
import tn.esprit.spring.services.EntrepriseServiceImpl;
import org.springframework.boot.test.context.SpringBootTest;
import org.junit.jupiter.api.Test;

import java.util.List;

import static org.junit.Assert.*;
import static org.junit.Assert.assertTrue;

@SpringBootTest
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)

class TimesheetApplicationTests {


	@Autowired
	EntrepriseRepository entrepriseRepository;
	@Autowired

	DepartementRepository departementRepository;
	@Autowired

	EntrepriseServiceImpl entrepriseService;

	@Autowired

	DepartementServiceImpl departementService;

	private static final Logger l = Logger.getLogger(EntrepriseServiceImpl.class);


	@Test
	public void testAjouterEntreprise() {
		Entreprise entreprise = new Entreprise("ksdds","jhs");
		Entreprise savedEntreprise = entrepriseRepository.save(entreprise);
		Departement departement = new Departement(2,"inesss");
		departementRepository.save(departement);
		l.info("Ajoute");

		assertNotNull(savedEntreprise);

	}

	@Test
	public void testaffecterDepartementAEntreprise() {
		Departement departement = new Departement(2,"inesss");
		Departement save = departementRepository.save(departement);
		entrepriseService.affecterDepartementAEntreprise(2,1);
		l.info("AFFETCE");

		assertNotNull(save);
	}
	@Test
	public void testdeleteEntrepriseById(){
		Entreprise enn = new Entreprise("delete","delete");
		entrepriseRepository.save(enn);
		entrepriseService.deleteEntrepriseById(enn.getId());
		Boolean en = entrepriseService.getEntrepriseByIdB(enn.getId());
		assertTrue(en);
		if(en == true ){
			l.info("DELETED");

		}else{
			l.error("Cannot delete it!!");
		}
	}


}
